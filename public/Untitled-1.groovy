{
    "process": {
        "id": "214797b7c22908afe49950f6",
        "inputs": {
            "input": [
                {
                    "id": "input",
                    "title": "input",
                    "description": "The features",
                    "input": [
                        {
                            "id": "input",
                            "required": true,
                            "title": "input",
                            "format": "GeoJSON",
                            "value": {
                                "type": "FeatureCollection",
                                "features": [
                                    {
                                        "type": "Feature",
                                        "properties": {
                                            "ap_code": "01",
                                            "ap_tn": "เมืองเชียงใหม่",
                                            "fertility": "-",
                                            "pH_top": "-",
                                            "pv_code": "50",
                                            "pv_tn": "เชียงใหม่",
                                            "seriesname": "พื้นที่ชุมชน",
                                            "soilgroup": "U",
                                            "soilseries": "U",
                                            "tb_code": "09",
                                            "tb_tn": "สุเทพ",
                                            "texture_to": "-",
                                            "doc_count": 2,
                                            "h3_id": "89648289a7bffff"
                                        },
                                        "geometry": {
                                            "coordinates": [
                                                [
                                                    [
                                                        98.95994226152669,
                                                        18.777531986970715
                                                    ],
                                                    [
                                                        98.959429594832,
                                                        18.779414737451496
                                                    ],
                                                    [
                                                        98.95743560193934,
                                                        18.779952549955723
                                                    ],
                                                    [
                                                        98.95595429161345,
                                                        18.77860760638159
                                                    ],
                                                    [
                                                        98.95646698086644,
                                                        18.776724852149577
                                                    ],
                                                    [
                                                        98.95846095788797,
                                                        18.776187045242427
                                                    ],
                                                    [
                                                        98.95994226152669,
                                                        18.777531986970715
                                                    ]
                                                ]
                                            ],
                                            "type": "Polygon"
                                        }
                                    },
                                    {
                                        "type": "Feature",
                                        "properties": {},
                                        "geometry": {
                                            "coordinates": [
                                                98.95039049099057,
                                                18.77606968020794
                                            ],
                                            "type": "Point"
                                        }
                                    },
                                    {
                                        "type": "Feature",
                                        "properties": {},
                                        "geometry": {
                                            "coordinates": [
                                                98.95578494030553,
                                                18.77424980038471
                                            ],
                                            "type": "Point"
                                        }
                                    },
                                    {
                                        "type": "Feature",
                                        "properties": {},
                                        "geometry": {
                                            "coordinates": [
                                                98.9522506459258,
                                                18.77483686053901
                                            ],
                                            "type": "Point"
                                        }
                                    }
                                ]
                            }
                        }
                    ]
                }
            ],
            "parameter": []
        },
        "outputs": [
            {
                "id": "result",
                "title": "result",
                "description": "The result features",
                "required": true,
                "format": "vallaris",
                "value": "65d424ec5702a6f837c8b01c",
                "transmissionMode": "value"
            }
        ],
        "response": "raw",
        "mode": "async",
        "createTemporary": false,
        "group": 1,
        "createdAt": "2023-10-24T04:59:24.320Z",
        "createdBy": "65374f2b285de6045d6ff690",
        "updatedAt": "2023-10-24T04:59:24.320Z",
        "updatedBy": "65374f2b285de6045d6ff690"
    }
}