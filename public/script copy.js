// script.js

// เลือกเอลิเมนต์ .sidebar และ .toggle-btn
const sidebar = document.querySelector('.sidebar');
const toggleBtn = document.querySelector('.toggle-btn');
const popup = document.querySelector('.popup');

// เพิ่มการฟังก์ชัน click เพื่อแสดง/ซ่อน sidebar
toggleBtn.addEventListener('click', () => {
    sidebar.classList.toggle('active');
});

/// สร้างฟังก์ชันสำหรับการเพิ่มข้อความใน popup เมื่อคลิกที่ลิงก์ "JSON"
document.querySelector(".list-item.active").addEventListener("click", function (event) {
    event.preventDefault(); // ป้องกันการเปลี่ยนเส้นทางหรือโหลดหน้าใหม่

    // ซ่อน Popup หากถูกแสดงอยู่แล้ว
    const popup = this.querySelector(".popup");
    if (popup.style.display === "block") {
        popup.style.display = "none";
    } else {
        // แสดง Popup
        popup.style.display = "block";

        // เมื่อพื้นที่ข้อความใน Popup ถูกคลิก ให้โฟกัสไปที่พื้นที่นั้น
        popup.focus();
    }
});

// เพิ่ม Event Listener เพื่อตรวจจับการกดปุ่ม Enter เพื่อเพิ่มข้อความ
document.querySelector(".list-item.active .popup").addEventListener("keydown", function (event) {
    // ตรวจสอบว่าปุ่ม Enter ถูกกดหรือไม่
    if (event.key === "Enter") {
        // ป้องกันการ submit form หรือการเปลี่ยนบรรทัดใน textarea
        event.preventDefault();

        // เพิ่มข้อความที่ผู้ใช้พิมพ์ลงใน Popup
        const text = this.textContent.trim(); // ดึงข้อความที่ผู้ใช้พิมพ์
        console.log("User entered:", text); // สามารถแทนด้วยการทำสิ่งที่คุณต้องการกับข้อความที่ผู้ใช้พิมพ์

        // ล้างข้อความใน Popup เมื่อพิมพ์เสร็จ
        this.textContent = "";
    }
});

document.getElementById('export-button').addEventListener('click', function (event) {
    event.preventDefault(); // ป้องกันการโหลดหน้าใหม่หรือการเปลี่ยนเส้นทาง

    // ถ้ามีข้อมูลใน popup
    if (popup.innerText.trim() !== "") {
        // แสดง alert ถามผู้ใช้ว่าต้องการบันทึกหรือไม่
        var confirmSave = confirm("คุณต้องการบันทึกข้อมูล GeoJSON ลงในเครื่องของคุณหรือไม่?");
        if (confirmSave) {
            // สร้าง Blob จากข้อความ GeoJSON
            const geoJSONData = popup.innerText;
            const blob = new Blob([geoJSONData], { type: 'application/json' });

            // สร้าง URL สำหรับ Blob
            const blobURL = window.URL.createObjectURL(blob);

            // สร้างลิงก์สำหรับดาวน์โหลดไฟล์
            const downloadLink = document.createElement('a');
            downloadLink.href = blobURL;
            downloadLink.download = 'data.geojson'; // กำหนดชื่อไฟล์ที่จะดาวน์โหลด
            downloadLink.click();

            // ล้าง URL หลังจากดาวน์โหลดเสร็จสิ้น
            window.URL.revokeObjectURL(blobURL);

            alert("ข้อมูลถูกบันทึกแล้ว!");
        } else {
            // ถ้าผู้ใช้ยกเลิกการบันทึก
            alert("การบันทึกถูกยกเลิก");
        }
    } else {
        // ถ้าไม่มีข้อมูลใน popup
        alert("ไม่มีข้อมูล GeoJSON ที่จะบันทึก");
    }
});

// // สร้างฟังก์ชันเมื่อคลิกที่ปุ่ม "New"
// document.getElementById('new-button').addEventListener('click', function (event) {
//     // รีเซ็ตทั้งหมด
//     resetAll();
// });

// // ฟังก์ชันสำหรับรีเซ็ตทั้งหมด
// function resetAll() {
//     // รีเซ็ตแท็บ JSON ให้ว่างเปล่า
//     resetJsonTab();

//     // ลบ geometries ทั้งหมดที่ถูกวาดบนแผนที่
//     draw.deleteAll(); // draw คือตัวแปรที่เก็บ MapboxDraw instance
// }

// // ฟังก์ชันสำหรับรีเซ็ตแท็บ JSON ให้ว่างเปล่า
// function resetJsonTab() {
//     var popup = document.querySelector('.popup');
//     popup.innerText = ''; // รีเซ็ตข้อความในแท็บ JSON ให้ว่างเปล่า
//     popup.style.display = 'none'; // ซ่อนแท็บ JSON
// }
function resetMap() {
    // Clear the GeoJSON display
    var popup = document.querySelector('.popup');
    popup.innerText = '';
    popup.style.display = 'none';

    // Remove all drawn geometries
    draw.deleteAll();
}

